/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
  Image,
  Platform,
  PermissionsAndroid,
} from 'react-native';

import RNFetchBlob from 'rn-fetch-blob';
// import CameraRoll from '@react-native-community/cameraroll';

import * as RNFS from 'react-native-fs';

export class App extends Component {
  constructor(props) {
    super();
    this.state = {
      imagePath: '',
    };
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.requestPermission();
    }
  }

  requestPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission',
          message: 'App needs access to your storage to download Photos.',
        },
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission',
          message: 'App needs access to your storage to view Photos.',
        },
      );
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert('Storage Permission Not Granted');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  downloadImage = () => {
    let imageUrl =
      'https://reactnativecode.com/wp-content/uploads/2018/02/motorcycle.jpg';
    const {config} = RNFetchBlob;
    let downloadDir = RNFetchBlob.fs.dirs.DownloadDir;
    let newImgUri = imageUrl.lastIndexOf('/');
    let imageName = imageUrl.substring(newImgUri);

    let path = downloadDir + '/.offline' + imageName;

    console.log(path);

    if (Platform.OS === 'android') {
      let options = {
        fileCache: true,
        path: path,
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          path: path,
        },
      };

      config(options)
        .fetch('GET', imageUrl)
        .then(res => {
          if (res) {
            this.setState({
              imagePath: res.path(),
            });
          }
          Alert.alert('Image Downloaded Successfully.');
        });
    } else {
      let iosPath = RNFS.DocumentDirectoryPath + '/.offline';
      RNFS.mkdir(iosPath);

      iosPath += imageName;

      const {id, promise} = RNFS.downloadFile({
        fromUrl: imageUrl,
        toFile: iosPath,
        background: false,
        cacheable: false,
      });

      promise
        .then(res => {
          if (res) {
            this.setState({
              imagePath: iosPath,
            });
          }
        })
        .catch(error => {
          if (error) {
          }
        });
    }
  };

  deleteImage = () => {
    RNFetchBlob.fs
      .unlink(this.state.imagePath)
      .then(res => {
        this.setState({
          imagePath: '',
        });
      })
      .catch(err => {});
  };

  render() {
    let url = this.state.imagePath;
    if (Platform.OS === 'android') {
      url = 'file://' + url;
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={this.downloadImage}>
          <Text style={styles.text}>Download</Text>
        </TouchableOpacity>
        <Image
          source={{uri: url}}
          style={{width: 300, height: 300, resizeMode: 'contain', margin: 5}}
        />
        <TouchableOpacity style={styles.button} onPress={this.deleteImage}>
          <Text style={styles.text}>Delete</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    width: '80%',
    paddingTop: 3,
    paddingBottom: 3,
    backgroundColor: '#2E7D32',
    borderRadius: 7,
    margin: 10,
  },
  text: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    padding: 5,
  },
});
export default App;
